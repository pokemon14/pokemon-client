import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {PokemonDetailsComponent} from "./components/pokemon-details/pokemon-details.component";
import {PokemonsListComponent} from "./components/pokemons-list/pokemons-list.component";

const routes: Routes = [
  {path: '', redirectTo: 'pokemons', pathMatch: 'full'},
  {path: 'pokemons', component: PokemonsListComponent},
  {path: 'pokemons/:id', component: PokemonDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component, Input, OnInit} from '@angular/core';
import {Pokemon} from "../../models/pokemon.model";
import {PokemonService} from 'src/app/services/pokemon.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.css']
})
export class PokemonDetailsComponent implements OnInit {
  @Input() viewMode = false;
  @Input() currentPokemon: Pokemon = {
    name: '',
    url: '',
    published: false
  };
  message = '';

  constructor(
    private pokemonService: PokemonService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getPokemon(this.route.snapshot.params["name"]);
    }
  }

  updatePublished(status: boolean): void {
    const data = {
      title: this.currentPokemon.name,
      description: this.currentPokemon.url,
      published: status
    };
    this.message = '';
    // this.pokemonService.update(this.currentPokemon.id, data)
    //   .subscribe({
    //     next: (res) => {
    //       console.log(res);
    //       this.currentPokemon.published = status;
    //       this.message = res.message ? res.message : 'The status was updated successfully!';
    //     },
    //     error: (e) => console.error(e)
    //   });
  }

  getPokemon(name: string): void {
    this.pokemonService.get(name)
      .subscribe({
        next: (data) => {
          this.currentPokemon = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
}

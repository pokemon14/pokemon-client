import {Component, OnInit} from '@angular/core';

import {Pokemon} from 'src/app/models/pokemon.model';
import {PokemonService} from "../../services/pokemon.service";

@Component({
  selector: 'app-pokemons-list',
  templateUrl: './pokemons-list.component.html',
  styleUrls: ['./pokemons-list.component.css']
})
export class PokemonsListComponent implements OnInit {

  pokemons?: Pokemon[];
  currentPokemon: Pokemon = {};
  currentIndex = -1;
  title = '';

  constructor(private pokemonService: PokemonService) {
  }

  ngOnInit(): void {
    this.retrievePokemons();
  }

  retrievePokemons(): void {
    this.pokemonService.getAll()
      .subscribe({
        next: (data) => {
          this.pokemons = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }

  setActivePokemon(pokemon: Pokemon, index: number): void {
    this.currentPokemon = pokemon;
    this.currentIndex = index;
  }
}

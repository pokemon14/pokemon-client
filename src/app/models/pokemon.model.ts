export class Pokemon {
  id?: any;
  name?: string;
  url?: string;
  published?: boolean = true;
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pokemon} from '../models/pokemon.model';

const baseUrl = 'http://localhost:8080/api/pokemons';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {
  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(baseUrl);
  }

  get(name: string): Observable<Pokemon> {
    return this.http.get(`${baseUrl}/${name}`);
  }

  findByTitle(title: any): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`${baseUrl}?title=${title}`);
  }
}
